# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end
every :hour do
  runner "Order.netsuiteBatch"
  runner "NetSuiteIntegration::activerecord_import [Promotion, Store]"
  runner "WarrantySubmission.submit_all_to_netsuite"
end

every 15.minutes do
  runner "ProductTranslation.update_netsuite"
end

every 6.hours do
  runner "Social.update_social"
  runner "NetSuiteIntegration::activerecord_import [NetsuiteState, NetsuiteCountry, WarrantyItem]"
end

every :day do
  runner "StoreController.new.setLatLng"
end

# Learn more: http://github.com/javan/whenever
