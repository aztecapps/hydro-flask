module NetSuiteIntegration
  class Command
    attr_accessor :command, :data, :response
    
    def initialize
      @command = ''
      @data = ''
      @response = nil
    end
    
    def post
      require 'net/http'
      require 'net/https'
      
      payload = {
        secret: "mysecret",
        command: @command,
        data: @data
      }
      
      uri = URI.parse("https://forms.netsuite.com/app/site/hosting/scriptlet.nl?script=customscript_webstoreintegration&deploy=customdeploy1&compid=1273675&h=32aae5b062ade13fdf60")
      https = Net::HTTP.new(uri.host, uri.port)
      https.use_ssl = true
      
      req = Net::HTTP::Post.new(uri)
      req['Content-Type'] = 'application/json'
      req['User-Agent'] = 'Mozilla/5.0'
      req.body = payload.to_json
      
      res = https.request(req)
      @response = JSON.parse(res.body)
      if @response['status'] == 'success'
        true
      else
        false
      end
    end
  end
  
  def self.activerecord_import(models)
    models.each do |model|
      command_name = model.import_command_name
      id_column = model.import_id_column
      import_columns = model.import_columns
      
      command = NetSuiteIntegration::Command.new
      command.command = command_name
      command.post
      if command.response['status'] == 'success'
        
        to_import = command.response['response']
        ActiveRecord::Base.transaction do
          #Upsert any records included in the import
          to_import.each do |id, columns|
            existing = model.find_by(id_column => id)
            if !existing
              column_hash = Hash.new
              column_hash[id_column] = id
              import_columns.each do |column_name|
                column_hash[column_name] = columns[column_name.to_s]
              end
              model.create(column_hash)
            else
              import_columns.each do |column_name|
                if existing.send(column_name) != columns[column_name.to_s]
                  existing.send("#{column_name}=", columns[column_name.to_s])
                end
              end
              existing.save
            end
          end
        
          #Delete any records not included in the import
          model.all.each do |record|
            id = record[id_column].to_s
            if !to_import[id]
              record.destroy
            end
          end
          
        end
      end
    end
    true
  end
end