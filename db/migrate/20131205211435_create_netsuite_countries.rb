class CreateNetsuiteCountries < ActiveRecord::Migration
  def change
    create_table :netsuite_countries do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
