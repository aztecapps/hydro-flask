class CreateWarrantySubmissions < ActiveRecord::Migration
  def change
    create_table :warranty_submissions do |t|
      t.string :first_name, :limit => 32
      t.string :last_name, :limit => 32
      t.string :company_name
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :country
      t.string :zip_code
      t.string :phone
      t.string :email
      t.date :purchase_date
      t.string :purchased_from
      t.string :netsuite_status

      t.timestamps
    end
    
    create_table :warranty_items do |t|
      t.string :item_name
      t.string :netsuite_id
      
      t.timestamps
    end
    
    create_table :warranty_claims do |t|
      t.references :warranty_submission
      t.references :warranty_item
      t.integer :quantity
      t.string :product_issue
      
      t.timestamps
    end
  end
end
