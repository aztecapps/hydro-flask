class AddStateNameToStateTaxRate < ActiveRecord::Migration
  def change
    add_column :state_tax_rates, :state_name, :string
  end
end
