class CreateNetsuiteStates < ActiveRecord::Migration
  def change
    create_table :netsuite_states do |t|
      t.string :netsuite_id
      t.string :name

      t.timestamps
    end
  end
end
