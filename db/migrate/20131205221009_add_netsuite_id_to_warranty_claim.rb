class AddNetsuiteIdToWarrantyClaim < ActiveRecord::Migration
  def change
    add_column :warranty_claims, :netsuite_id, :string
  end
end
