class AddNetsuiteIdToStore < ActiveRecord::Migration
  def change
    add_column :stores, :netsuite_id, :string
  end
end
