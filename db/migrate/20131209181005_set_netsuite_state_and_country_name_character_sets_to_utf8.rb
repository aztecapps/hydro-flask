class SetNetsuiteStateAndCountryNameCharacterSetsToUtf8 < ActiveRecord::Migration
  def change
    execute "ALTER TABLE `netsuite_states` CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ;"
    execute "ALTER TABLE `netsuite_countries` CHANGE COLUMN `name` `name` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ;"
  end
end

