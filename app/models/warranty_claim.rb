class WarrantyClaim < ActiveRecord::Base
  attr_accessible :quantity, :product_issue, :warranty_submission_id, :warranty_item_id, :netsuite_id
  
  belongs_to :warranty_submission
  belongs_to :warranty_item
  
  validates :quantity, presence: true, numericality: { only_integer: true, greater_than: 0 }
  validates :product_issue, presence: true
  validates :warranty_item_id, presence: true
  validates :warranty_submission, presence: true
  
  before_save do
    if self.warranty_item
      self.netsuite_id = self.warranty_item.netsuite_id
    end
  end
end
