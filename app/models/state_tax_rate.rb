class StateTaxRate < ActiveRecord::Base
  attr_accessible :state_acronym, :tax_rate, :state_name
end
