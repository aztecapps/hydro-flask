class WarrantyItem < ActiveRecord::Base
  attr_accessible :item_name, :netsuite_id
  
  def self.import_command_name
    "get_item_name_mappings"
  end
  
  def self.import_id_column
    :netsuite_id
  end
  
  def self.import_columns
    [:item_name]
  end
end
