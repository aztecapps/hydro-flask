class Store < ActiveRecord::Base
  attr_accessible :address1, :address2, :city, :country, :lat, :lng, :name, :phone, :state, :zip, :netsuite_id
  
  before_save do
    changes = self.changed
    if 
      changes.include? "address1" or
      changes.include? "address2" or
      changes.include? "city" or
      changes.include? "state" or
      changes.include? "zip" or
      changes.include? "country"
        self.lat = self.lng = 0
        self.set_lat_lng
    end
  end
  
  def set_lat_lng
    address = "#{self.address1} #{self.address2} #{self.city} #{self.state} #{self.zip} #{self.country}"
    coords = Store.get_coords_for_address(address)
    if coords
      self.lat = coords["lat"]
      self.lng = coords["lng"]
    end
  end
  
  def self.get_coords_for_address(address)
    sleep 1 #Don't call Google too fast
    require 'net/http'
 		address = Rack::Utils.escape(address)
    url = "http://maps.googleapis.com/maps/api/geocode/json?address=#{address}&sensor=false"
    url_data = Net::HTTP.get(URI.parse(url))
    json_data = JSON.parse(url_data)
    if !json_data["results"].nil? && !json_data["results"].empty?
    	return json_data["results"][0]["geometry"]["location"]
    end
    return false
 	end
  
  def self.get_stores_by_coords(lat, lng, max_stores=20)
  	Store.select("id, name, address1, address2, city, state, zip, phone, country, lat, lng, ( 3959 * acos( cos( radians(#{lat}) ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(#{lng}) ) + sin( radians(#{lat}) ) * sin( radians( lat ) ) ) ) AS distance").order("distance asc").limit(max_stores).where("lat <> ? AND lng <> ?", 0, 0)
  end

  def self.get_stores_by_address(address, max_stores=20)
    coords = Store.get_coords_for_address(address)
    if coords
      Store.get_stores_by_coords(coords["lat"], coords["lng"], max_stores)
    else
      false
    end
  end
  
  def self.import_command_name
    "get_store_locations"
  end
  
  def self.import_id_column
    :netsuite_id
  end
  
  def self.import_columns
    [:address1, :address2, :city, :country, :name, :phone, :state, :zip]
  end

end