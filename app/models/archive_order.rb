class ArchiveOrder < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :credit_card_number, :address, :address2, :city, :state, :zip, :phone, :email,
                  :billing_address, :billing_address2, :billing_city, :billing_state, :billing_zip, 
                  :shipping_method_id, :invoice_number, :status, :account_id, :total_amount,
                  :payment_total_cost, :shipping_cost, :tax, :netsuite_status, :promotion_id
  
              
  has_many :archive_order_line_items
  belongs_to :account
  belongs_to :shipping, foreign_key: 'shipping_method_id'

end
