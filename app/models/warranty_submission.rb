class WarrantySubmission < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :company_name, :address1, :address2, :city, :state, :zip_code, :country, :email, :phone, :purchase_date, :purchased_from, :warranty_claims_attributes
  
  has_many :warranty_claims, dependent: :destroy, inverse_of: :warranty_submission
  accepts_nested_attributes_for :warranty_claims, reject_if: :all_blank
  
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :first_name, presence: true, length: { maximum: 32 }
  validates :last_name, presence: true, length: { maximum: 32 }
  validates :address1, presence: true
  validates :city, presence: true
  validates :state, presence: true
  validates :zip_code, presence: true
  validates :country, presence: true
  validates :phone, presence: true
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }
  validates :purchase_date, presence: true, timeliness: { type: :date, on_or_before: Time.now }
  validates :purchased_from, presence: true
  
  validates_associated :warranty_claims
  
  after_initialize :init
  
  def init
    self.netsuite_status ||= "New"
  end
  
  def submit_to_netsuite
    command = NetSuiteIntegration::Command.new
    command.command = 'warrantyformsubmission'
    
    claims = []
    self.warranty_claims.each do |claim|
      claims.push claim.as_json(methods: :warranty_item_netsuite_id)
    end
    
    command.data = {
      submission: self.as_json(methods: :purchase_date_netsuite),
      claims: claims
    }
    
    begin
      if command.post
        self.netsuite_status = "Imported"
      else
        self.netsuite_status = "Error"
        puts command.response['errors']
      end
      self.save
    rescue Exception => e
      puts e
    end 
  end
  
  def self.submit_all_to_netsuite
    WarrantySubmission.where(netsuite_status: 'New').each do |submission|
      submission.submit_to_netsuite
    end
  end
  
  def purchase_date_netsuite
    self.purchase_date.strftime("%m/%d/%Y")
  end
end
