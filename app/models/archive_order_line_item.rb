class ArchiveOrderLineItem < ActiveRecord::Base

  attr_accessible :product_id, :netsuite_id, :quantity, :product_name, :product_image_id, :product_price, :product_subtotal, :weight

  belongs_to :product
  belongs_to :product_image
  belongs_to :archive_order

  validates :product_id, presence: true
  validates :quantity, presence: true
  validates :product_image_id, presence: true
  validates :product_price, presence: true
  validates :product_subtotal, presence: true

end
