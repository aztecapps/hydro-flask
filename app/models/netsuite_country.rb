class NetsuiteCountry < ActiveRecord::Base
  attr_accessible :code, :name
  
  def self.import_command_name
    "get_netsuite_countries"
  end
  
  def self.import_id_column
    :code
  end
  
  def self.import_columns
    [:name]
  end
end
