class Promotion < ActiveRecord::Base

  attr_accessible :promo_code, :start_date, :end_date, :amount, :promo_type, :netsuite_id

  def getPromo(promo)
  	this_promo = Promotion.where("promo_code = ? and CURDATE() between start_date AND end_date", promo).first
  	return this_promo
  end
  
  def self.import_command_name
    "get_promo_codes"
  end
  
  def self.import_id_column
    :netsuite_id
  end
  
  def self.import_columns
    [:promo_code, :start_date, :end_date, :amount, :promo_type]
  end
  
end