class NetsuiteState < ActiveRecord::Base
  attr_accessible :netsuite_id, :name
  
  def self.import_command_name
    "get_netsuite_states"
  end
  
  def self.import_id_column
    :netsuite_id
  end
  
  def self.import_columns
    [:name]
  end
end
