require 'will_paginate/array'

class StoresController < ApplicationController
  # GET /stores
  # GET /stores.json
  caches_page :index, :show
  
  def index
    @stores = Store.get_stores_by_address(params[:location], 20)
    if @stores
      @stores = @stores.paginate(page: params[:page], per_page: 4) if !@stores.blank?
    else
      @stores = []
      @error_message = "We’re sorry, Google Maps employees are currently setting fresh tracks at Mt. Bachelor Ski Resort and are unable to process your request. Please check back shortly."
    end
    @location = params[:location]
    respond_to do |format|
      format.html # index.html.erb
    end
  end

  # GET /stores/1
  # GET /stores/1.json
  def show
    @store = Store.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @store }
    end
  end

  def setLatLng
    require 'net/http'
    stores = Store.where("lat = ? and lng = ?", 0, 0)
    stores.each do |store|
      store.set_lat_lng
      store.save
    end
  end

  # GET /stores/new
  # GET /stores/new.json
  def new
    @store = Store.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @store }
    end
  end

  # GET /stores/1/edit
  def edit
    @store = Store.find(params[:id])
  end

  # POST /stores
  # POST /stores.json
  def create
    @store = Store.new(params[:store])

    respond_to do |format|
      if @store.save
        setLatLng
        @store = Store.find(params[:id])
        format.html { redirect_to @store, notice: 'Store was successfully created.' }
        format.json { render json: @store, status: :created, location: @store }
      else
        format.html { render action: "new" }
        format.json { render json: @store.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /stores/1
  # PUT /stores/1.json
  def update
    @store = Store.find(params[:id])

    respond_to do |format|
      if @store.from_json(request.body)
        @store.save
        setLatLng
        @store = Store.find(params[:id])
        format.html { redirect_to @store, notice: 'Store was successfully updated.' }
        format.json { render json: @store }
      else
        format.html { render action: "edit" }
        format.json { render json: @store.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stores/1
  # DELETE /stores/1.json
  def destroy
    @store = Store.find(params[:id])
    @store.destroy

    respond_to do |format|
      format.html { redirect_to stores_url }
      format.json { head :no_content }
    end
  end
end
