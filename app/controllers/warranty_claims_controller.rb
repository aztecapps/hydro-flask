class WarrantyClaimsController < ApplicationController
  # GET /warranty_claims
  # GET /warranty_claims.json
  def index
    @warranty_claims = WarrantyClaim.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @warranty_claims }
    end
  end

  # GET /warranty_claims/1
  # GET /warranty_claims/1.json
  def show
    @warranty_claim = WarrantyClaim.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @warranty_claim }
    end
  end

  # GET /warranty_claims/new
  # GET /warranty_claims/new.json
  def new
    @warranty_claim = WarrantyClaim.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @warranty_claim }
    end
  end

  # GET /warranty_claims/1/edit
  def edit
    @warranty_claim = WarrantyClaim.find(params[:id])
  end

  # POST /warranty_claims
  # POST /warranty_claims.json
  def create
    @warranty_claim = WarrantyClaim.new(params[:warranty_claim])

    respond_to do |format|
      if @warranty_claim.save
        format.html { redirect_to @warranty_claim, notice: 'Warranty Claim was successfully created.' }
        format.json { render json: @warranty_claim, status: :created, location: @warranty_claim }
      else
        format.html { render action: "new" }
        format.json { render json: @warranty_claim.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /warranty_claims/1
  # PUT /warranty_claims/1.json
  def update
    @warranty_claim = WarrantyClaim.find(params[:id])

    respond_to do |format|
      if @warranty_claim.update_attributes(params[:warranty_claim])
        format.html { redirect_to @warranty_claim, notice: 'Warranty Claim was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @warranty_claim.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warranty_claims/1
  # DELETE /warranty_claims/1.json
  def destroy
    @warranty_claim = WarrantyClaim.find(params[:id])
    @warranty_claim.destroy

    respond_to do |format|
      format.html { redirect_to warranty_claims_url }
      format.json { head :no_content }
    end
  end
end
