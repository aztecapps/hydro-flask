class BrandsController < ApplicationController

  def show

    @archive_order = ArchiveOrder.where("id = ? and account_id = ? ", params[:id], current_account.id).first

    respond_to do |format|
      format.html
      format.json { render json: @archive_order }
    end
  end

end
