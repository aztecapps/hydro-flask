class WarrantySubmissionsController < ApplicationController
  def prebuild_warranty_claims (warranty_submission)
    while warranty_submission.warranty_claims.length < 4
      warranty_submission.warranty_claims.build
    end
  end
  
  # GET /warranty_submissions
  # GET /warranty_submissions.json
  def index
    @warranty_submissions = WarrantySubmission.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @warranty_submissions }
    end
  end

  # GET /warranty_submissions/1
  # GET /warranty_submissions/1.json
  def show
    @warranty_submission = WarrantySubmission.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @warranty_submission }
    end
  end

  # GET /warranty_submissions/new
  # GET /warranty_submissions/new.json
  def new
    @warranty_submission = WarrantySubmission.new
    prebuild_warranty_claims @warranty_submission

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @warranty_submission }
    end
  end

  # GET /warranty_submissions/1/edit
  def edit
    @warranty_submission = WarrantySubmission.find(params[:id])
  end

  # POST /warranty_submissions
  # POST /warranty_submissions.json
  def create
    @warranty_submission = WarrantySubmission.new(params[:warranty_submission])

    respond_to do |format|
      if @warranty_submission.save
        format.html { redirect_to warranty_thankyou_path, notice: 'Your warranty form was successfully submitted.' }
        format.json { render json: @warranty_submission, status: :created, location: @warranty_submission }
      else
        prebuild_warranty_claims @warranty_submission
        format.html { render action: "new" }
        format.json { render json: @warranty_submission.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def thanks
  end

  # PUT /warranty_submissions/1
  # PUT /warranty_submissions/1.json
  def update
    @warranty_submission = WarrantySubmission.find(params[:id])

    respond_to do |format|
      if @warranty_submission.update_attributes(params[:warranty_submission])
        format.html { redirect_to @warranty_submission, notice: 'Warranty Submission was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @warranty_submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /warranty_submissions/1
  # DELETE /warranty_submissions/1.json
  def destroy
    @warranty_submission = WarrantySubmission.find(params[:id])
    @warranty_submission.destroy

    respond_to do |format|
      format.html { redirect_to warranty_submissions_url }
      format.json { head :no_content }
    end
  end
end
