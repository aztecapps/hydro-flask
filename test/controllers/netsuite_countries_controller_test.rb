require 'test_helper'

class NetsuiteCountriesControllerTest < ActionController::TestCase
  setup do
    @netsuite_country = netsuite_countries(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:netsuite_countries)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create netsuite_country" do
    assert_difference('NetsuiteCountry.count') do
      post :create, netsuite_country: { code: @netsuite_country.code, name: @netsuite_country.name }
    end

    assert_redirected_to netsuite_country_path(assigns(:netsuite_country))
  end

  test "should show netsuite_country" do
    get :show, id: @netsuite_country
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @netsuite_country
    assert_response :success
  end

  test "should update netsuite_country" do
    patch :update, id: @netsuite_country, netsuite_country: { code: @netsuite_country.code, name: @netsuite_country.name }
    assert_redirected_to netsuite_country_path(assigns(:netsuite_country))
  end

  test "should destroy netsuite_country" do
    assert_difference('NetsuiteCountry.count', -1) do
      delete :destroy, id: @netsuite_country
    end

    assert_redirected_to netsuite_countries_path
  end
end
