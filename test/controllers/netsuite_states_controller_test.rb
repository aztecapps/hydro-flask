require 'test_helper'

class NetsuiteStatesControllerTest < ActionController::TestCase
  setup do
    @netsuite_state = netsuite_states(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:netsuite_states)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create netsuite_state" do
    assert_difference('NetsuiteState.count') do
      post :create, netsuite_state: { name: @netsuite_state.name, netsuite_id: @netsuite_state.netsuite_id }
    end

    assert_redirected_to netsuite_state_path(assigns(:netsuite_state))
  end

  test "should show netsuite_state" do
    get :show, id: @netsuite_state
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @netsuite_state
    assert_response :success
  end

  test "should update netsuite_state" do
    patch :update, id: @netsuite_state, netsuite_state: { name: @netsuite_state.name, netsuite_id: @netsuite_state.netsuite_id }
    assert_redirected_to netsuite_state_path(assigns(:netsuite_state))
  end

  test "should destroy netsuite_state" do
    assert_difference('NetsuiteState.count', -1) do
      delete :destroy, id: @netsuite_state
    end

    assert_redirected_to netsuite_states_path
  end
end
